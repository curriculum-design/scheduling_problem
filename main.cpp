#include "pch.h"
#include <string>
#include <iostream>
#include <vector>
using namespace std;

class condition
{
public:
	condition(string name) :name(name) {}
	string name;
	int arrSub = 0;
	vector<int> con;
	void push(int i) { con.push_back(i); }
	int getDay() { return con.at(arrSub); }
};

vector<condition>allCondition;

condition& addCondition(string name)
{
	allCondition.push_back(condition(name));
	return allCondition.back();
}

void display()
{
	for (condition& c : allCondition)
		cout << c.name << c.getDay();
	cout << endl;
}

bool findOrNext(condition& c, int sub)
{
	if (c.arrSub == c.con.size())
	{
		c.arrSub = 0;
		return false;
	}

	for (int i = 0; i < sub; i++)
	{
		if (allCondition.at(i).getDay() == c.getDay()) ///不能和前面别人的一样
		{
			c.arrSub++; //如果有冲突就尝试下一天
			return findOrNext(c, sub);
		}
	}
	return true;
}

void recu(condition& c, int sub)
{
	for(c.arrSub = 0; findOrNext(c, sub); c.arrSub++)
	{
		if (sub == allCondition.size() - 1) //到了最后一个
		{
			display();
			return;
		}
		else
		{
			recu(allCondition.at(sub + 1), sub + 1);
			//前面的情况不行或者已经探索完了，探索下一种情况
		}
	}
}

int main()
{
	condition& c2 = addCondition("赵");
	c2.push(2);
	c2.push(4);
	condition& c1 = addCondition("钱");
	c1.push(1);
	c1.push(6);
	condition& c3 = addCondition("孙");
	c3.push(3);
	c3.push(7);
	condition& c4 = addCondition("李");
	c4.push(5);
	condition& c5 = addCondition("周");
	c5.push(1);
	c5.push(4);
	c5.push(6);
	condition& c6 = addCondition("吴");
	c6.push(2);
	c6.push(5);
	condition& c7 = addCondition("陈");
	c7.push(3);
	c7.push(6);
	c7.push(7);

	recu(allCondition.at(0), 0);
}